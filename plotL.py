import ROOT
from ROOT import *
from array import array
import math
import time
import os
import shutil
import collections



##################################################################
#################### General definitions #########################
##################################################################
DSIDs_ttbar  = [410501,410511,410512,410225,410525]

var_names = ["BDT", "mBB", "pTJ3", "dPhiVBB", "pTB1", "pTB2", "MET", "mBBJ", "dRBB", "dEtaBB", "150_200ptv_SR_mBBCutBased", "150_200ptv_SR_METCutBased", "150_200ptv_SR_dRBBCutBased", "200ptv_SR_mBBCutBased", "200ptv_SR_METCutBased", "200ptv_SR_dRBBCutBased"]
var_names_for_fit = ["mBB", "MET"]

jets = ["2","3"]
##################################################################
##################################################################
##################################################################



##################################################################
######################### Graphics ###############################
##################################################################
colors = {} 
colors["361"] = kRed
colors["364"] = kBlack
colors["364 PTV 1up CS Sys"] = kOrange+2
colors["364 PTV 1down CS Sys"] = kOrange+2
colors["364 MBB 1up CS Sys"] = kAzure+9
colors["364 MBB 1down CS Sys"] = kAzure+9
colors["364 MUR0.5_MUF0.5_PDF261000"] = kViolet 
colors["364 MUR0.5_MUF1_PDF261000"] = kOrange+2
colors["364 MUR1_MUF0.5_PDF261000"] = kOrange+2
colors["364 MUR1_MUF1_PDF13000"] = kViolet
colors["364 MUR1_MUF1_PDF25300"] = kViolet
colors["364 MUR1_MUF1_PDF261098"] = kRed
colors["364 MUR1_MUF2_PDF261000"] = kAzure+9
colors["364 MUR2_MUF1_PDF261000"] = kAzure+9
colors["364 MUR2_MUF2_PDF261000"] = kViolet
colors["410501"] = kBlack
colors["410511"] = kGreen
colors["410512"] = kOrange 
colors["410225"] = kRed
colors["410525"] = kViolet
colors["410501 PTV 1up CS Sys"] = kOrange+2
colors["410501 PTV 1down CS Sys"] = kOrange+2
colors["410501 MBB 1up CS Sys"] = kAzure+9
colors["410501 MBB 1down CS Sys"] = kAzure+9

line_style = {}
line_style["361"] = 1
line_style["364"] = 1
line_style["364 PTV 1up CS Sys"] = 1
line_style["364 PTV 1down CS Sys"] = 1
line_style["364 MBB 1up CS Sys"] = 1
line_style["364 MBB 1down CS Sys"] = 1
line_style["364 MUR0.5_MUF0.5_PDF261000"] = 1
line_style["364 MUR0.5_MUF1_PDF261000"] = 1
line_style["364 MUR1_MUF0.5_PDF261000"] = 1
line_style["364 MUR1_MUF1_PDF13000"] = 1
line_style["364 MUR1_MUF1_PDF25300"] = 1
line_style["364 MUR1_MUF1_PDF261098"] = 1
line_style["364 MUR1_MUF2_PDF261000"] = 1
line_style["364 MUR2_MUF1_PDF261000"] = 1
line_style["364 MUR2_MUF2_PDF261000"] = 1
line_style["410501"] = 1
line_style["410511"] = 1
line_style["410512"] = 1
line_style["410225"] = 1
line_style["410525"] = 1
line_style["410501 PTV 1up CS Sys"] = 1
line_style["410501 PTV 1down CS Sys"] = 1
line_style["410501 MBB 1up CS Sys"] = 1
line_style["410501 MBB 1down CS Sys"] = 1

line_width = {}
line_width["361"] = 3
line_width["364"] = 3
line_width["364 PTV 1up CS Sys"] = 2
line_width["364 PTV 1down CS Sys"] = 2
line_width["364 MBB 1up CS Sys"] = 2
line_width["364 MBB 1down CS Sys"] = 2
line_width["364 MUR0.5_MUF0.5_PDF261000"] = 2
line_width["364 MUR0.5_MUF1_PDF261000"] = 2
line_width["364 MUR1_MUF0.5_PDF261000"] = 2
line_width["364 MUR1_MUF1_PDF13000"] = 2
line_width["364 MUR1_MUF1_PDF25300"] = 2 
line_width["364 MUR1_MUF1_PDF261098"] = 2
line_width["364 MUR1_MUF2_PDF261000"] = 2
line_width["364 MUR2_MUF1_PDF261000"] = 2
line_width["364 MUR2_MUF2_PDF261000"] = 2
line_width["410501"] = 3
line_width["410511"] = 3
line_width["410512"] = 3
line_width["410225"] = 3
line_width["410525"] = 3
line_width["410501 PTV 1up CS Sys"] = 2
line_width["410501 PTV 1down CS Sys"] = 2
line_width["410501 MBB 1up CS Sys"] = 2
line_width["410501 MBB 1down CS Sys"] = 2

DSIDName = {}
DSIDName[361] = "MadGraph"
DSIDName[364] = "Sherpa"
DSIDName[410501] = "Pow+Pyt8"
DSIDName[410511] = "Pow+Pyt8RLow"
DSIDName[410512] = "Pow+Pyt8RHigh"
DSIDName[410225] = "aMC@NLo+Pyt8"
DSIDName[410525] = "Pow+Herwig7"

var_range = collections.defaultdict(dict)
var_range["BDT"]["min"] = -1.
var_range["BDT"]["max"] = 1.
var_range["mBB"]["min"] = 0. 
var_range["mBB"]["max"] = 500.
var_range["pTJ3"]["min"] = 0.
var_range["pTJ3"]["max"] = 210.
var_range["dPhiVBB"]["min"] = 3.0
var_range["dPhiVBB"]["max"] = 3.2
var_range["pTB1"]["min"] = 0.  
var_range["pTB1"]["max"] = 300.
var_range["pTB2"]["min"] = 0.  
var_range["pTB2"]["max"] = 120.
var_range["MET"]["min"] = 150.
var_range["MET"]["max"] = 300.
var_range["mBBJ"]["min"] = 0.  
var_range["mBBJ"]["max"] = 1000.
var_range["dRBB"]["min"] = 0  
var_range["dRBB"]["max"] = 5.
var_range["dEtaBB"]["min"] = 0.
var_range["dEtaBB"]["max"] = 5.
var_range["150_200ptv_SR_mBBCutBased"]["min"] = 0.
var_range["150_200ptv_SR_mBBCutBased"]["max"] = 500.
var_range["150_200ptv_SR_METCutBased"]["min"] = 150.
var_range["150_200ptv_SR_METCutBased"]["max"] = 200.
var_range["150_200ptv_SR_dRBBCutBased"]["min"] = 0
var_range["150_200ptv_SR_dRBBCutBased"]["max"] = 5.
var_range["200ptv_SR_mBBCutBased"]["min"] = 0.
var_range["200ptv_SR_mBBCutBased"]["max"] = 500.
var_range["200ptv_SR_METCutBased"]["min"] = 200.
var_range["200ptv_SR_METCutBased"]["max"] = 300.
var_range["200ptv_SR_dRBBCutBased"]["min"] = 0
var_range["200ptv_SR_dRBBCutBased"]["max"] = 5.
##################################################################
##################################################################
##################################################################



##################################################################
######################## Auxiliary functions #####################
##################################################################
def computeNewBin(histo,target):

    #Let's consinder an example: mBB.
    bins=[]
    bins.append(histo.GetXaxis().GetXmax())          #500. Store in the bins the position of the edge of the last bin to the right.
    theBin=histo.FindBin(histo.GetXaxis().GetXmax()) #101. TheBin will decrease.
    refBin=histo.GetNbinsX()                         #100. The refBin is a constant, until the relative error is below the target.

    while theBin>1 :
        theBin-=1                                      #Go down in the number of bins, starting from the overflow.
        err=Double(0)                                  #Reutrn 0 double 
        intV=histo.IntegralAndError(theBin,refBin,err) #Integral (sums the entries without multiplying by dx) between theBin and RefBin. The error here it is computed assuming uncorrelation between the bin.
        if intV==0:                                    #If there are no events in the bin, loop again including one bin further left.
            continue
        relError=100
        if intV!=0 and intV>0.0000001: relError=err/intV                        #Calculate the relative error if there are events in the bin.
        if relError<target:                                                     #If the relative error in this new bin is samller than the target...
            refBin=theBin-1                                                     #... the new refBin is the one after theBin (which was the one that allow to reach the target).
            bins.append(histo.GetBinCenter(theBin)-histo.GetBinWidth(theBin)/2) #Save the left edge of the bin.

    err=Double(0)                                           #This is the automatic rebinning of the first bin on the left
    intV=histo.IntegralAndError(-1,refBin,err)
    if intV!=0 and intV>0.000001 and err/intV>target:
        del bins[-1]
    bins.append(histo.GetXaxis().GetXmin())
    bins=list(set(bins))
    bins.sort()                                            #Before, we were adding the bins in the reverse order, e.g. 500, 400, 350, 320...
    return bins
##################################################################
def rebinHisto(histo, bins, xmin = -1, xmax = -1, divide_by_bin_width = True):

    if(xmin > -1):                                                                       #Set an external maximum and minimum
      for left_bin_edge in bins:
        if xmin >= left_bin_edge:
          bins = bins[bins.index(left_bin_edge):]

    if(xmax > -1):
      for right_bin_edge in reversed(bins):
        if xmax <= right_bin_edge:
          bins = bins[:bins.index(right_bin_edge)+1]

    xaxis = array( 'd', bins )
    newH=TH1D("hist","hist",len(bins)-1,xaxis)                                          #Creates a new histogram with the desired bins.
    newH.SetDirectory(0)
    for bin in xrange(0,histo.GetNbinsX()+1):                                           #Loop over the entries of the old histogram                        
      val=histo.GetBinContent(bin)
      err=histo.GetBinError(bin)
      newBin=newH.FindBin(histo.GetBinCenter(bin))                                    #Find in which bin of the new histogram the value of the old histogram should be put
      if newBin> newH.GetNbinsX(): newBin= newH.GetNbinsX()                           #Put the overflow bin in the last bin
      if newBin<1                : newBin= 1                                          #Put the underflow bin in the first bin
      newH.SetBinContent(newBin, val+newH.GetBinContent(newBin) )                     
      newH.SetBinError(newBin, math.sqrt(err*err+pow(newH.GetBinError(newBin),2)) )   
    if(divide_by_bin_width):
      for bin in xrange(0,newH.GetNbinsX()+1):                                            #All the bins are divided by the bin width!
        newH.SetBinContent(bin,newH.GetBinContent(bin)/newH.GetBinWidth(bin))
        newH.SetBinError(bin,newH.GetBinError(bin)/newH.GetBinWidth(bin))

    return newH, bins
##################################################################
def getHisto(file_path, var_name_in_file, normalise = False):
  file_histo = ROOT.TFile(file_path)
  histo = file_histo.Get(var_name_in_file)
  histo.SetDirectory(0)
  file_histo.Close()
  if(normalise == True):
    histo.Scale(1./histo.Integral(0,-1))
  return histo
#################################################################
def getBinList(histo):
  xmin = histo.GetXaxis().GetXmin()
  xmax = histo.GetXaxis().GetXmax()

  bins = []
  bins.append(xmin)

  x = xmin
  bin_number = 0
  while(x < xmax):
    x+= histo.GetXaxis().GetBinWidth(bin_number)
    bins.append(x)
    bin_number+= 1

  return bins
##################################################################
def createDir(output_dir):
  if os.path.exists(output_dir):
    shutil.rmtree(output_dir)
  os.makedirs(output_dir)
  os.chdir(output_dir)
##################################################################
##################################################################
##################################################################



##################################################################
##################### Class for plotting #########################
##################################################################

class HistoL:

  def __init__(self, DSID, file_path, var_name_in_file, target = 1, rebin = 1, do_ratio_plot = True, normalise = False, normalise_to_histo = False, ratio_of_integrals = False, set_y_min = -1, set_y_max = -1, set_y_ratioplot_min = 0.5, set_y_ratioplot_max = 1.5, do_fit = False, divide_by_bin_width = True, relative_error_plot = False):

    self.DSID = DSID

    for var in var_names:
      if var in var_name_in_file:
        self.var_name = var 

    self.jet_selection = -1
    if("2jet" in var_name_in_file): 
      self.jet_selection = 2
    if("3jet" in var_name_in_file): 
      self.jet_selection = 3

    if "CutBased" in self.var_name:
      var_name_in_file = var_name_in_file.replace("150ptv_SR_","")

    self.Sys = ""
    if("V__1up_CS" in var_name_in_file):
      self.Sys = " PTV 1up CS Sys"
    if("V__1down_CS" in var_name_in_file):
      self.Sys = " PTV 1down CS Sys"
    if("MBB__1up_CS" in var_name_in_file) or ("Mbb__1up_CS" in var_name_in_file):
      self.Sys = " MBB 1up CS Sys"
    if("MBB__1down_CS" in var_name_in_file) or ("Mbb__1down_CS" in var_name_in_file):
      self.Sys = " MBB 1down CS Sys"
    if("MUR0.5_MUF0.5_PDF261000" in var_name_in_file):
      self.Sys = " MUR0.5_MUF0.5_PDF261000"
    if("MUR0.5_MUF1_PDF261000" in var_name_in_file):
      self.Sys = " MUR0.5_MUF1_PDF261000"
    if("MUR1_MUF0.5_PDF261000" in var_name_in_file):
      self.Sys = " MUR1_MUF0.5_PDF261000"
    if("MUR1_MUF1_PDF13000" in var_name_in_file):
      self.Sys = " MUR1_MUF1_PDF13000"
    if("MUR1_MUF1_PDF25300" in var_name_in_file):
      self.Sys = " MUR1_MUF1_PDF25300"
    if("MUR1_MUF1_PDF261098" in var_name_in_file):
      self.Sys = " MUR1_MUF1_PDF261098"
    if("MUR1_MUF2_PDF261000" in var_name_in_file):
      self.Sys = " MUR1_MUF2_PDF261000"
    if("MUR2_MUF1_PDF261000" in var_name_in_file):
      self.Sys = " MUR2_MUF1_PDF261000"
    if("MUR2_MUF2_PDF261000" in var_name_in_file):
      self.Sys = " MUR2_MUF2_PDF261000"

    #Retrieve the histogram
    file_histo = ROOT.TFile(file_path)
    self.histo = file_histo.Get(var_name_in_file)
    self.histo.SetDirectory(0)
    file_histo.Close()

    #Save the value of the integral before the rebinning, in which you divide the bin content by the width, and the normalisation
    self.integral = self.histo.Integral(0,-1)

    #Rebinning
    if (isinstance(target, float) and (rebin == 1)):
      #For the first histo, rebin according to a max error. Then adjust min and max of the histo.
      self.bins = computeNewBin(self.histo, target)
      results = rebinHisto(self.histo, self.bins, var_range[self.var_name]["min"], var_range[self.var_name]["max"], divide_by_bin_width)
      self.histo = results[0]
      self.bins  = results[1] 
    elif (isinstance(target, int) and (target > 0) and (rebin == 1)):
      #Option for not rebinning or merging bins keeping them of equal size among them. Then adjust min and max of the histo.
      self.histo = self.histo.Rebin(target)
      temporary_bins_list = getBinList(self.histo)
      results = rebinHisto(self.histo, temporary_bins_list, var_range[self.var_name]["min"], var_range[self.var_name]["max"], divide_by_bin_width)
      self.histo = results[0]
      self.bins  = results[1] 
    else:
      #Rebin the histogram as the first one. 
      results = rebinHisto(self.histo, rebin, var_range[self.var_name]["min"], var_range[self.var_name]["max"], divide_by_bin_width)
      self.histo = results[0]
      self.bins  = results[1]

    #Normalise
    if(normalise == True):
      self.histo.Scale(1./self.histo.Integral(0,-1))

    #Normalise to histo
    if(normalise_to_histo != False):
      self.histo.Divide(normalise_to_histo)

    #Show the ratio in the legend of the integrals, most likely systematic vs nominal. Ratio_of_integrals from a TH1F will become a float.
    if(ratio_of_integrals != False):
      numerator = ratio_of_integrals.Integral(0,-1) 
      denominator = self.integral
      ratio_of_integrals = numerator / denominator
      ratio_of_integrals = format(ratio_of_integrals, '.2f')
      ratio_of_integrals = " Nom / Sys Integrals: " + str(ratio_of_integrals)  
    else:
      ratio_of_integrals = ""
    self.ratio_of_integrals = ratio_of_integrals

    #Do ratio plot
    if(do_ratio_plot == True):
      self.do_ratio_plot = True
      self.y_ratioplot_min = set_y_ratioplot_min
      self.y_ratioplot_max = set_y_ratioplot_max
    else:
      self.do_ratio_plot = False

    #Error plot
    if(relative_error_plot == True):
      for bin_index in range(0,self.histo.GetNbinsX()+1): #+2: to include the overflow bin (+1) and since range exclude the last element.
        if self.histo.GetBinError(bin_index) == 0:
          rel_error = 0
        else:
          rel_error = self.histo.GetBinError(bin_index) / self.histo.GetBinContent(bin_index)
        self.histo.SetBinContent(bin_index, rel_error)
        self.histo.SetBinError(bin_index, 0)
      

    #Graphics
    self.histo.SetLineColor(colors[str(DSID) + self.Sys])
    self.histo.SetLineStyle(line_style[str(DSID) + self.Sys])
    self.histo.SetLineWidth(line_width[str(DSID) + self.Sys])
    if(set_y_min != -1):
      self.histo.SetMinimum(set_y_min)
    if(set_y_max != -1):
      self.histo.SetMaximum(set_y_max)

    #Set axis titles
    if(relative_error_plot == True):
      self.histo.GetYaxis().SetTitle("Rel. Error")
    elif(normalise == False):
      self.histo.GetYaxis().SetTitle("Yields")
    else:
      self.histo.GetYaxis().SetTitle("a.u.")
    self.histo.GetYaxis().SetTitleOffset(1.5)

    #Set the unit of measurment
    if(("mBB" in self.var_name) or ("MET" in self.var_name) or self.var_name == "pTJ3" or self.var_name == "pTB1" or self.var_name == "pTB2" or self.var_name == "mBBJ"):
      self.unit = "[GeV]"
    else:
      self.unit = ""

    #HL name
    self.name = str(self.DSID) + "_" + str(self.var_name)
    if(self.jet_selection > -1):
      self.name += "_" + str(self.jet_selection) + "jet"

    #Legend position
    self.x1_legend = 0.6
    self.y1_legend = 0.7
    self.x2_legend = 0.9
    self.y2_legend = 0.9
    if(self.var_name == "dPhiVBB" or self.var_name == "pTB1"):
      self.x1_legend = 0.15
      self.y1_legend = 0.7
      self.x2_legend = 0.45
      self.y2_legend = 0.9    

    #Fit
    self.do_fit = do_fit
    self.fitFunc = TF1("fitFunc","[0] + x*[1]", self.histo.GetXaxis().GetXmin(), self.histo.GetXaxis().GetXmax())
    self.fitFunc.SetParameters(1, 0)
    if(self.do_fit):
      if(self.var_name == "MET"):
        self.fitFunc = TF1("fitFunc" + str(DSID),"[0] + x*[1]", self.histo.GetXaxis().GetXmin(), self.histo.GetXaxis().GetXmax())
        self.fitFunc.SetParameters(1, 0.001)
        self.fitFunc.SetLineColor(colors[str(DSID) + self.Sys])
        self.histo.Fit(self.fitFunc)
      if(self.var_name == "mBB"):
        self.fitFunc = TF1("fitFunc" + str(DSID),"[0] + [1]*exp([2]*x)", self.histo.GetXaxis().GetXmin(), self.histo.GetXaxis().GetXmax())
        self.fitFunc.SetParameters(0.917, 0.389, -0.0138e-3)
        self.fitFunc.SetLineColor(colors[str(DSID) + self.Sys])
        self.histo.Fit(self.fitFunc)

##################################################################
##################################################################
##################################################################



##################################################################
########################## Plotting ##############################
##################################################################

def plot (plot_sample):

  gStyle.SetOptStat(0)
  gStyle.SetOptTitle(kFALSE)
  ROOT.gROOT.SetBatch(True)

  for var, HLs in plot_sample.items():

      #Canvas
      can=TCanvas(var, var, 800,800)

      #First plot flag
      first_plot = True
      histo_denominator = 0    
      ratio_plots = []

      for HL in HLs:

        #Sys plot without errors
        draw_without_error = ""

        #Main plots
        if(first_plot):
          if(HL.do_ratio_plot):
            #Pad 1
            pad_1=TPad("pad_1", "up", 0., 0.28, 1., 1.);
            pad_1.SetBottomMargin(0);
            pad_1.Draw();
            #Pad 2 for ratio plot
            pad_2=TPad("pad_2", "down", 0.0, 0.00, 1.0, 0.28);
            pad_2.SetTopMargin(0);
            pad_2.SetBottomMargin(0.32);
            pad_2.Draw();
            pad_1.cd()
          HL.histo.Draw()
        else:
          if(HL.do_ratio_plot):
            pad_1.cd()
          #Draw other histograms
          if(len(HL.Sys) > 1):
            draw_without_error = "hist"
          HL.histo.Draw("SAME" + draw_without_error)

        #Draw the fit if present
        if(HL.do_fit):
          HL.fitFunc.Draw("SAME")
          #X axis
          HL.histo.GetXaxis().SetTitle(HL.var_name + " " + HL.unit)
          HL.histo.GetXaxis().SetTitleOffset(1.0)

        #Legend
        if(first_plot):
          leg = TLegend(HL.x1_legend, HL.y1_legend, HL.x2_legend, HL.y2_legend)
          leg.SetShadowColor(10)
          leg.SetFillStyle(0)
          leg.SetBorderSize(0)
          atlasLabel = TLatex()
          atlasLabel.SetTextFont(42)
          atlasLabel.SetNDC();
          atlasLabel.DrawLatex(HL.x1_legend, HL.y1_legend - 0.05, "#scale[0.8]{#bf{#it{ATLAS}} Internal}");
        leg.AddEntry(HL.histo, DSIDName[HL.DSID] + HL.Sys + HL.ratio_of_integrals, "lpe")

        #Ratio plot
        if(HL.do_ratio_plot):
          if(first_plot):
            histo_denominator = HL.histo.Clone("denominator")
          else:
            ratio = HL.histo.Clone("numerator" + HL.var_name + draw_without_error)
            ratio.Divide(histo_denominator)
            #Y axis
            ratio.SetMaximum(HL.y_ratioplot_max)
            ratio.SetMinimum(HL.y_ratioplot_min)
            ratio.GetYaxis().SetTitleSize(0.12)
            ratio.GetYaxis().SetTitleOffset(0.4)
            ratio.GetYaxis().SetTitle("1 / Nom")
            ratio.GetYaxis().SetLabelSize(0.05)
            #X axis
            ratio.GetXaxis().SetTitle(HL.var_name + " " + HL.unit)
            ratio.GetXaxis().SetTitleSize(0.13)
            ratio.GetXaxis().SetTitleOffset(1.0)                
            ratio.GetXaxis().SetLabelSize(0.08)
            ratio_plots.append(ratio)

        #After the plot has been drawn, set the flag first plot to false
        first_plot = False

      #Draw the legend
      leg.Draw("SAME")

      if(len(ratio_plots) != 0):
        #Draw the plots (you need a second loop for memory problems)
        pad_2.cd()
        for ratio_plot in ratio_plots:
          draw_without_error = ""
          if("hist" in  ratio_plot.GetName()):
            draw_without_error = " hist"
          ratio_plot.Draw("SAME" + draw_without_error)

        #Draw the line
        line1=TLine(HL.bins[0], 1., HL.bins[-1], 1.)
        line1.SetLineColor(922)
        line1.SetLineWidth(2)
        line1.SetLineStyle(2)
        line1.Draw("SAME")

      #Save the plots
      can.SaveAs(var + ".png")
##################################################################
##################################################################
##################################################################
