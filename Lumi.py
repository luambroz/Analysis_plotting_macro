################
#Valerio's macro
################

import ROOT
import os
import sys
from glob import glob
###from sets import Set
import time
import math

ROOT.gStyle.SetOptStat(0)

#year=16
#year=17
year=18

giuliaFile=None
if year==16: giuliaFile=ROOT.TFile("../All_a_d/Reader_0L_32-07_a_MVA_D1/fetch/data-MVATree/data15-16.root")
if year==17: giuliaFile=ROOT.TFile("../All_a_d/Reader_0L_32-07_d_MVA_D1/fetch/data-MVATree/data17.root")
if year==18: giuliaFile=ROOT.TFile("../All_e/Reader_0L_32-07-P_e_MVA_D1/fetch/data-MVATree/data18.root")

giuliaTree=giuliaFile.Get("Nominal")


file="ilumicalc_histograms_None_297730-311481_OflLumi-13TeV-005.root"
if year==17: file="physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"
if year==18: file="ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-001.root"

file15="ilumicalc_histograms_None_276262-284484_OflLumi-13TeV-005.root "
lFile=ROOT.TFile(file)
lFile15=ROOT.TFile(file15)

runs=[]
##########################################################################
### get the runList from the ilumiCalc file
histoList= lFile.GetListOfKeys()
iterator=ROOT.TIter(histoList)
obj=iterator()
while obj!=None:
    name=obj.GetName()
    if "_intlumi" in name: 
        ##print name
        runs.append(name)
    obj=iterator()

if year==16:
    histoList= lFile15.GetListOfKeys()
    iterator=ROOT.TIter(histoList)
    obj=iterator()
    while obj!=None:
        name=obj.GetName()
        if "_intlumi" in name: 
            ##print name
            runs.append(name)
            pass
        obj=iterator()
        runs.sort()
print (runs)
print (len(runs))


histo=ROOT.TH1D("Lumi","Lumi",len(runs),-0.5,len(runs)-0.5)
histo.Sumw2()
histo.SetDirectory(0)
histo.SetMinimum(0)
histo.SetMaximum(10)
#histo.SetMinimum(30)
#histo.SetMaximum(70)
total=0
count=0
TOTcount=0
for run in runs: 
    count+=1
    runNB=run.split("un")[1].split("_int")[0]
    #print run+" --> "+runNB
    hist=None
    if float(runNB)>284484:
        lFile.cd()
        hist=lFile.Get(run)
    else:
        lFile15.cd()
        hist=lFile15.Get(run)
    if hist==None: lFile.ls()
    intLumi=0
    for bin in range(0,hist.GetNbinsX()+10):
        cont=hist.GetBinContent(bin)
        if cont!=0: intLumi=cont/1e6
    total+=intLumi
count=0
    
#giuliaFile.cd()
TMPhistV=ROOT.TH1D( "Htot", "Htot", 10,0,5000000) 
giuliaTree.Draw("RunNumber>>Htot","RunNumber>0 && nTags>0 ","goff")
TOTcount=TMPhistV.Integral()
TOTcount/=total


print (" ")
print (" ")
print ("TOTAL INTEGRATED luminosity is: "+str(total))
print ("average ev/pb-1: "+str(TOTcount))
print (" ")
#sys.exit()


TotalLumi = 0
for run in runs:
    count+=1
    runNB=run.split("un")[1].split("_int")[0]
    #print run+" --> "+runNB
    hist=None
    if float(runNB)>284484:
        lFile.cd()
        hist=lFile.Get(run)
    else:
        lFile15.cd()
        hist=lFile15.Get(run)
    if hist==None: lFile.ls()
    intLumi=0
    for bin in range(0,hist.GetNbinsX()+10):
        cont=hist.GetBinContent(bin)
        if cont!=0: intLumi=cont/1e6
    histo.GetXaxis().SetBinLabel(count,runNB)
    #total+=intLumi
    #giuliaFile.cd()
    TMPhist=ROOT.TH1D( "H"+runNB, "H"+runNB, 10,0,5000000) 
    giuliaTree.Draw("RunNumber>>H"+runNB," RunNumber=="+runNB+" && nTags>0 ","goff")
    histo.SetBinContent( count, float(TMPhist.Integral()) )

    histo.SetBinError( count, math.sqrt(float(TMPhist.Integral())) )
    histo.SetBinContent( count, histo.GetBinContent(count)/float(intLumi) ) 
    histo.SetBinError( count, histo.GetBinError(count)/float(intLumi) )

    if histo.GetBinContent(count)==0: 
        print (" RUN: "+runNB+" with Lumi: "+str(intLumi)+"    IS MISSING <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<")
        pass
    elif histo.GetBinContent(count)<0.80*TOTcount:
        print (" RUN: "+runNB+" has Lumi: "+str(intLumi)+"   HAS integral: "+str(histo.GetBinContent(count))+" which is: "+str(histo.GetBinContent(count)/TOTcount)+"  of the average "+str(TOTcount))
        pass
    else:
        #print " RUN: "+runNB+" has Lumi: "+str(intLumi)+"   HAS integral: "+str(histo.GetBinContent(count))
        TotalLumi+=intLumi
        pass
    

    #print '+run'+runNB+'_$(parent) { <.cutExpression=" EventInfo.runNumber()=='+runNB+'", .weightExpression = "1./'+str(intLumi)+'"> }'

print(" ")    
print("TotalLumi:" + str(TotalLumi))
print(" ")

myC=ROOT.TCanvas("pair per run","pair per run",1200,600)
histo.SetTitle("; ;events/pb^{-1}")
histo.SetMarkerStyle(20)
histo.SetMarkerSize(0.1)
histo.SetMarkerColor(2)
histo.SetLineColor(2)
histo.SetLineWidth(2)
histo.Draw("E");
oFile=ROOT.TFile("profile.root","RECREATE")
histo.Write()
oFile.Close()

myC.Print("LumiProfile_"+str(year)+".pdf")

print (" ")
print (" ")

lFile.Close()
lFile15.Close()
#giuliaFile.Close()

time.sleep(10)
