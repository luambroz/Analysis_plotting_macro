import os
import plotL

#Create an output directory (if it exists, remove it and create a new one). Then move into it.
plotL.createDir("ttbar/")

ttbar = {}
#####
for jet in plotL.jets:
  for var in plotL.var_names:

    if(var == "pTJ3" and jet == "2"):
      continue
    if(var == "mBBJ" and jet == "2"):
      continue

    ###The rebin var will help spread the desired binning to the other histo. At the beginning leave it to 1###
    rebin = 1

    entry = str(jet) + "jet_" +var
    ttbar[entry] = []

    for DSID in plotL.DSIDs_ttbar:

      HL = plotL.HistoL(
                        DSID,
                        "/home/ambroz/VHbb/VHbbTruthFramework_v2/run/OutputDir" + str(DSID) + "/result" + str(DSID) + ".root",
                        "hist_0Lep_ttbar_2tag" + str(jet) + "jet_150ptv_SR_" + var + "_Nominal",
                        0.05, #target
                        rebin,
                        do_ratio_plot = True
                       )

      #Rebin all the systematic like the first one
      rebin = HL.bins

      ttbar[entry].append(HL)
#####
plotL.plot(ttbar)
os.chdir("../")