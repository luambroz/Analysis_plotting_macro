###a###
cd All_a/Reader_0L_32-02_a_MVA_D1/fetch/
rm hist.root
hadd hist.root hist-*
cd -

cd All_a/Reader_0L_32-02_a_MVA_T2/fetch/
rm hist.root
hadd hist.root hist-*
cd -
#######

###d###
cd All_d/Reader_0L_32-02_d_MVA_D1/fetch/
rm hist.root
hadd hist.root hist-*
cd -

cd All_d/Reader_0L_32-02_d_MVA_T2/fetch/
rm hist.root
hadd hist.root hist-*
cd -
#######

###e###
cd All_e/Reader_0L_32-02_e_MVA_D1/fetch/
rm hist.root
hadd hist.root hist-*
cd -

cd All_e/Reader_0L_32-02_e_MVA_T2/fetch/
rm hist.root
hadd hist.root hist-*
cd -
#######

###Tot###
rm hist_a.root
rm hist_d.root
hadd hist_a.root All_a/Reader_0L_32-02_a_MVA_D1/fetch/hist.root All_a/Reader_0L_32-02_a_MVA_T2/fetch/hist.root
hadd hist_d.root All_d/Reader_0L_32-02_d_MVA_D1/fetch/hist.root All_d/Reader_0L_32-02_d_MVA_T2/fetch/hist.root
hadd hist_e.root All_e/Reader_0L_32-02_e_MVA_D1/fetch/hist.root All_e/Reader_0L_32-02_e_MVA_T2/fetch/hist.root

rm hist_ad.root
hadd hist_ad.root hist_a.root hist_d.root

rm hist_ade.root
hadd hist_ade.root hist_ad.root hist_e.root
#########