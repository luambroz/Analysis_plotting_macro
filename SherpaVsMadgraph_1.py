import os
import plotL

#Create an output directory (if it exists, remove it and create a new one). Then move into it.
plotL.createDir("ZplusJets_S_M_v1/")

sherpa_vs_madgraph = {}
#####
for jet in plotL.jets:
  for var in plotL.var_names:

    if(var == "pTJ3" and jet == "2"):
      continue
    if(var == "mBBJ" and jet == "2"):
      continue

    ###Set the max error of the first histo, then copy binning to the others###
    target = 4

    entry = str(jet) + "jet_" +var
    sherpa_vs_madgraph[entry] = []

    HLS = plotL.HistoL(
                      364, #Beginning of Sherpa DSIDs
                      "/home/ambroz/VHbb/VHbbTruthFramework_v2/run/OutputDirSherpa/output_0Lep.root",
                      "hist_0Lep_Zbb_2tag" + str(jet) + "jet_150ptv_SR_" + var + "_Nominal",
                      target,
                      normalise = True,
                      do_ratio_plot = True
                      )
    sherpa_vs_madgraph[entry].append(HLS)

    #Rebin to Sherpa
    rebin = HLS.bins

    HLM = plotL.HistoL(
                      361, #Beginning of MadGraph DSIDs
                      "/home/ambroz/VHbb/VHbbTruthFramework_v2/run/OutputDirMadGraph/output_0Lep.root",
                      "hist_0Lep_Zbb_2tag" + str(jet) + "jet_150ptv_SR_" + var + "_Nominal",
                      rebin = rebin,
                      normalise = True,
                      do_ratio_plot = True
                       )
    sherpa_vs_madgraph[entry].append(HLM)
#####
plotL.plot(sherpa_vs_madgraph)
os.chdir("../")