import ROOT
from ROOT import *
from array import array
import math
import os

import multiprocessing
from multiprocessing import Pool, cpu_count


gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(True)


#####################BDT Rebinning 0 Lep##########################
BDT_bins = {}
BDT_bins["2"] = [0, 27, 106, 214, 361, 475, 548, 600, 641, 679, 712, 742, 771, 802, 839, 1002] #To update it, look in WSMaker here: fit_output/logs/output.log
BDT_bins["3"] = [0, 99, 184, 277, 370, 450, 524, 584, 635, 679, 718, 753, 787, 821, 861, 1002]

def remapBDTHisto(histo, bins_indices):

  #Here the code is quite hard coded
  histo_rebinned = TH1F("Rebin", "Rebin", 15, -1, 1)
  bin_edges = []
  bin_edges.append(-1)
  count=0
  for bin_index in bins_indices:
    count+=1
    if count==1: continue
    bin_edges.append(histo.GetBinCenter(bin_index)+0.001)
  xaxis = array( 'd', bin_edges )
  histo = histo.Rebin(len(bin_edges)-1, "histo_rebinned", xaxis)
  for bin_index in range(15+1):
    histo_rebinned.SetBinContent(bin_index, histo.GetBinContent(bin_index))
    histo_rebinned.SetBinError(bin_index, histo.GetBinError(bin_index))

  histo = histo_rebinned.Clone("v")

  return histo
##################################################################
def computeNewBin(histo,target):

    #Let's consinder an example: mBB.
    bins=[]
    bins.append(histo.GetXaxis().GetXmax())          #500. Store in the bins the position of the edge of the last bin to the right.
    theBin=histo.FindBin(histo.GetXaxis().GetXmax()) #101. TheBin will decrease.
    refBin=histo.GetNbinsX()                         #100. The refBin is a constant, until the relative error is below the target.

    while theBin>1 :
        theBin-=1                                      #Go down in the number of bins, starting from the overflow.
        err=Double(0)                                  #Reutrn 0 double 
        intV=histo.IntegralAndError(theBin,refBin,err) #Integral (sums the entries without multiplying by dx) between theBin and RefBin. The error here it is computed assuming uncorrelation between the bin.
        if intV==0:                                    #If there are no events in the bin, loop again including one bin further left.
            continue
        relError=100
        if intV!=0 and intV>0.0000001: relError=err/intV                        #Calculate the relative error if there are events in the bin.
        if relError<target:                                                     #If the relative error in this new bin is samller than the target...
            refBin=theBin-1                                                     #... the new refBin is the one after theBin (which was the one that allow to reach the target).
            bins.append(histo.GetBinCenter(theBin)-histo.GetBinWidth(theBin)/2) #Save the left edge of the bin.

    err=Double(0)                                           #This is the automatic rebinning of the first bin on the left
    intV=histo.IntegralAndError(-1,refBin,err)
    if intV!=0 and intV>0.000001 and err/intV>target:
        del bins[-1]
    bins.append(histo.GetXaxis().GetXmin())
    bins=list(set(bins))
    bins.sort()                                            #Before, we were adding the bins in the reverse order, e.g. 500, 400, 350, 320...
    return bins
##################################################################
def rebinHisto(histo, bins, xmin = -1, xmax = -1, divide_by_bin_width = True):

    if(xmin > -1):                                                                       #Set an external maximum and minimum
      for left_bin_edge in bins:
        if xmin >= left_bin_edge:
          bins = bins[bins.index(left_bin_edge):]

    if(xmax > -1):
      for right_bin_edge in reversed(bins):
        if xmax <= right_bin_edge:
          bins = bins[:bins.index(right_bin_edge)+1]

    xaxis = array( 'd', bins )
    newH=TH1D("hist","hist",len(bins)-1,xaxis)                                          #Creates a new histogram with the desired bins.
    newH.SetDirectory(0)
    for bin in xrange(0,histo.GetNbinsX()+1):                                           #Loop over the entries of the old histogram                        
      val=histo.GetBinContent(bin)
      err=histo.GetBinError(bin)
      newBin=newH.FindBin(histo.GetBinCenter(bin))                                    #Find in which bin of the new histogram the value of the old histogram should be put
      if newBin> newH.GetNbinsX(): newBin= newH.GetNbinsX()                           #Put the overflow bin in the last bin
      if newBin<1                : newBin= 1                                          #Put the underflow bin in the first bin
      newH.SetBinContent(newBin, val+newH.GetBinContent(newBin) )                     
      newH.SetBinError(newBin, math.sqrt(err*err+pow(newH.GetBinError(newBin),2)) )   
    if(divide_by_bin_width):
      for bin in xrange(0,newH.GetNbinsX()+1):                                            #All the bins are divided by the bin width!
        newH.SetBinContent(bin,newH.GetBinContent(bin)/newH.GetBinWidth(bin))
        newH.SetBinError(bin,newH.GetBinError(bin)/newH.GetBinWidth(bin))

    return newH, bins
##################################################################







def plotSample(sample):
  
  #Remove canvas warnings
  ROOT.gErrorIgnoreLevel = ROOT.kWarning  

  ################
  normalise = True
  ################
  
  tags = ["0ptag0pjet","2tag2jet","2tag3jet"]
  var_names = ["njets", "NFwdJets", "nCentralJetsNoBtag", "mva", "MET", "mBB", "dRBB", "pTB1", "pTB2", "ActualMu"]

  file_a = ROOT.TFile("../hist_a.root")
  file_d = ROOT.TFile("../hist_d.root")
  file_e = ROOT.TFile("../hist_e.root")
  fileICHEP_a = ROOT.TFile("/home/ambroz/VHbb/CxAODICHEP_Tag/CxAODFramework_tag_r31-16_1/run/hist_a.root")
  fileICHEP_d = ROOT.TFile("/home/ambroz/VHbb/CxAODICHEP_Tag/CxAODFramework_tag_r31-16_1/run/hist_d.root")

  for tag in tags:
    for var in var_names:
    
      full_name = sample + "_" + tag + "_150ptv_SR_" + var
      h_a = file_a.Get(full_name)
      h_d = file_d.Get(full_name)
      h_e = file_e.Get(full_name)    
      hICHEP_a = fileICHEP_a.Get(full_name)
      hICHEP_d = fileICHEP_d.Get(full_name)
    
      try:
        integral_a = h_a.Integral(0,-1)
        integral_d = h_d.Integral(0,-1)
        integral_e = h_e.Integral(0,-1)
        integralICHEP_a = hICHEP_a.Integral(0,-1)
        integralICHEP_d = hICHEP_d.Integral(0,-1)
      except AttributeError:
        continue

      #if var == "mva" and tag == "2tag2jet":
      #  print("==============================")
      #  print("Sample: " + sample)
      #  print("Yields 2tag2jet mc16a new: " + str(integral_a))
      #  print("Yields 2tag2jet mc16d new: " + str(integral_d))
      #  print("Yields 2tag2jet mc16e new: " + str(integral_e))
      #  print("Yields 2tag2jet mc16a ICHEP: " + str(integralICHEP_a))
      #  print("Yields 2tag2jet mc16d ICHEP: " + str(integralICHEP_d))
      #  print("==============================")

      if normalise:
        h_a.Scale(1./h_a.Integral(0,-1))
        h_d.Scale(1./h_d.Integral(0,-1))
        h_e.Scale(1./h_e.Integral(0,-1))
        hICHEP_a.Scale(1./hICHEP_a.Integral(0,-1))
        hICHEP_d.Scale(1./hICHEP_d.Integral(0,-1))

      if "2jet" in full_name:
        jet = "2"
      else:
        jet = "3"

      if "mva" in full_name:
        h_a = remapBDTHisto(h_a, BDT_bins[jet])
        h_d = remapBDTHisto(h_d, BDT_bins[jet])
        h_e = remapBDTHisto(h_e, BDT_bins[jet])
        hICHEP_a = remapBDTHisto(hICHEP_a, BDT_bins[jet])
        hICHEP_d = remapBDTHisto(hICHEP_d, BDT_bins[jet])
      elif "mBB" in full_name:
        target = 0.99
        x_min = 50
        x_max = 220
      elif "pTB1" in full_name:
        target = 0.9
        x_min = 0
        x_max = 350
      elif "pTB2" in full_name:
        target = 0.99
        x_min = 0
        x_max = 200
      elif "dRBB" in full_name:
        target = 0.3
        x_min = 0
        x_max = 3
      elif "MET" in full_name:
        target = 0.3
        x_min = 0
        x_max = 400
      elif "nCentral" in full_name:
        target = 0.99
        x_min = 0
        x_max = 7

      #Put target to 0.99 for example for no rebinning

      if "BB" in full_name or "pTB" in full_name or "MET" in full_name or "nCentral" in full_name:
        bins_a = computeNewBin(h_a, target)
        results_a = rebinHisto(h_a, bins_a, x_min, x_max, True)
        h_a = results_a[0]
        bins_a = results_a[1]
        results_d = rebinHisto(h_d, bins_a, x_min, x_max, True)
        h_d = results_d[0]
        bins_d = results_d[1]
        results_e = rebinHisto(h_e, bins_a, x_min, x_max, True)
        h_e = results_e[0]
        bins_e = results_e[1]
        resultsICHEP_a = rebinHisto(hICHEP_a, bins_a, x_min, x_max, True)
        hICHEP_a = resultsICHEP_a[0]
        binsICHEP_a = resultsICHEP_a[1]
        resultsICHEP_d = rebinHisto(hICHEP_d, bins_a, x_min, x_max, True)
        hICHEP_d = resultsICHEP_d[0]
        binsICHEP_d = resultsICHEP_d[1]


      h_a.SetLineWidth(2)
      h_d.SetLineWidth(2)
      h_e.SetLineWidth(2)
      hICHEP_a.SetLineWidth(2)
      hICHEP_d.SetLineWidth(2)

      h_a.SetLineColor(kRed)
      h_d.SetLineColor(kBlue+1)
      h_e.SetLineColor(kGreen+2)
      hICHEP_a.SetLineColor(kOrange-3)
      hICHEP_d.SetLineColor(kAzure+9)

      name = full_name.replace("/","")
      h_a.SetTitle(name)

      if "mva" in full_name:
        leg = TLegend(0.12, 0.67, 0.40, 0.90)
      else:
        leg = TLegend(0.58, 0.58, 0.90, 0.85)
      leg.SetShadowColor(10)
      leg.SetFillStyle(0)
      leg.SetBorderSize(0)
      leg.AddEntry(h_a, "mc16a", "lp")

      if integral_d/integral_a - 1 > 0:
        leg.AddEntry(h_d, "mc16d (+" + str(round(integral_d/integral_a - 1,3)*100) + "% mc16a)", "lp")
      else: 
        leg.AddEntry(h_d, "mc16d (" + str(round(integral_d/integral_a - 1,3)*100) + "% mc16a)", "lp")
      if integral_e/integral_a - 1 > 0:
        leg.AddEntry(h_e, "mc16e (+" + str(round(integral_e/integral_a - 1,3)*100) + "% mc16a)", "lp")
      else:
        leg.AddEntry(h_e, "mc16e (" + str(round(integral_e/integral_a - 1,3)*100) + "% mc16a)", "lp")
      if integralICHEP_a/integral_a  - 1 > 0:
        leg.AddEntry(hICHEP_a, "mc16a ICHEP (+" + str(round(integralICHEP_a/integral_a - 1,3)*100) + "% mc16a)", "lp")
      else:
        leg.AddEntry(hICHEP_a, "mc16a ICHEP (" + str(round(integralICHEP_a/integral_a - 1,3)*100) + "% mc16a)", "lp")
      if integralICHEP_d/integral_a  - 1 > 0:
        leg.AddEntry(hICHEP_d, "mc16d ICHEP (+" + str(round(integralICHEP_d/integral_a - 1,3)*100) + "% mc16a)", "lp")
      else:
        leg.AddEntry(hICHEP_d, "mc16d ICHEP (" + str(round(integralICHEP_d/integral_a - 1,3)*100) + "% mc16a)", "lp")

      c = TCanvas("2","2",800,900)
      pad_1=TPad("pad_1", "up", 0., 0.45, 1., 1.)
      pad_1.SetBottomMargin(0.02)
      pad_1.Draw()
      if "mva" in full_name:
        pad_1.SetLogy()
      pad_2=TPad("pad_2", "down", 0.0, 0.0, 1.0, 0.45);
      pad_2.SetTopMargin(0.02);
      pad_2.SetBottomMargin(0.6);
      pad_2.Draw();

      pad_1.cd()
      h_a.SetLabelSize(0)
      h_a.GetYaxis().SetLabelSize(0.04)
      h_a.GetYaxis().SetTitleSize(0.045)
      h_a.GetYaxis().SetTitleOffset(1.2)
      if "NoWeight" in full_name or "Presel" in full_name:
        h_a.GetYaxis().SetTitle("Number of events")
      else:
        h_a.GetYaxis().SetTitle("Yields")
      if normalise:
        h_a.GetYaxis().SetTitle("a.u.")
      h_a.Draw()
      h_d.Draw("SAME")
      h_e.Draw("SAME")
      hICHEP_a.Draw("SAME")
      hICHEP_d.Draw("SAME")
      leg.Draw("SAME")

      pad_2.cd()
      ratio_d = h_d.Clone("ratio")
      ratio_d.Divide(h_a)
      #ratio_d.GetXaxis().LabelsOption("v")
      ratio_d.GetXaxis().SetLabelSize(0.07)
      ratio_d.GetYaxis().SetTitleSize(0.04)    
      ratio_d.GetYaxis().SetTitle("1 / mc16a")
      ratio_d.SetTitle("")
      ratio_d.SetMinimum(0.5)
      ratio_d.SetMaximum(1.5)
      ratio_d.Draw("HIST")
      ratio_d.GetXaxis().SetLabelSize(0.07)

      ratio_e = h_e.Clone("ratio")
      ratio_e.Divide(h_a)
      ratio_e.Draw("HIST SAME")

      ratioICHEP_a = hICHEP_a.Clone("ratio")
      ratioICHEP_a.Divide(h_a)
      ratioICHEP_a.Draw("HIST SAME")

      ratioICHEP_d = hICHEP_d.Clone("ratio")
      ratioICHEP_d.Divide(h_a)
      ratioICHEP_d.Draw("HIST SAME")

      if "BB" in full_name or "pTB" in full_name or "MET" in full_name:
        line3=TLine(bins_a[0], 1, bins_a[-1], 1)
      else:
        line3=TLine(ratio_d.GetBinLowEdge(1), 1, ratio_d.GetBinLowEdge(ratio_d.GetNbinsX()+1), 1)
      line3.SetLineColor(922)
      line3.SetLineStyle(2)
      line3.SetLineWidth(1)
      line3.Draw("SAME")

      if not os.path.exists(sample):
        os.makedirs(sample)
     
      c.SaveAs(sample + "/" + name + ".png")
      c.SaveAs(sample + "/" + name + ".pdf")
    
      del c
      del h_a
      del h_d
      del h_e
      del hICHEP_a
      del hICHEP_d
      del ratio_d
      del ratio_e
      del ratioICHEP_a
      del ratioICHEP_d

  file_a.Close()
  file_d.Close()
  file_e.Close()
  fileICHEP_a.Close()
  fileICHEP_d.Close()

  return sample


##################################################################
sample_names = ["qqZvvH125", "qqZllH125", "qqWlvH125", "ggZvvH125", "ggZllH125",
                "ttbar",
                "Zbb", "Zbc", "Zbl", "Zcc", "Zcl", "Zl",
                "Wbb", "Wbc", "Wbl", "Wcc", "Wcl", "Wl",
                "stops","stopt", "stopWt",
                "WW","WZ","ZZ",
                "data"
                ]     
##################################################################


if __name__ == '__main__':
    
    #With num_cores > 8 there are too many threads reading the same file
    num_cores = multiprocessing.cpu_count()
    p = Pool(8)
    p.map(plotSample, sample_names)
    print("Done!")