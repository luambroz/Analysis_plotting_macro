from __future__ import division
import ROOT
from ROOT import *
from array import array
import math
import os
import sys

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import rc


def getBinVal(histo):
  nbins = histo.GetNbinsX()

  bin_number = 1

  x = []
  y = []
  while(bin_number <= nbins):
    y.append(histo.GetBinContent(bin_number))
    x.append(histo.GetBinCenter(bin_number))
    bin_number+= 1

  return x,y



plot_name = "dR_bj_min_plus_h_dR_bj_max"

file_output_qqZvvHbb = ROOT.TFile("qqZvvHbb_output.root")
h_FSR = file_output_qqZvvHbb.Get("h_qqZvvHbb_" + plot_name + "_FSR")
h_PU  = file_output_qqZvvHbb.Get("h_qqZvvHbb_" + plot_name + "_PU")
h_low = file_output_qqZvvHbb.Get("h_qqZvvHbb_" + plot_name + "_low")
h_HS  = file_output_qqZvvHbb.Get("h_qqZvvHbb_" + plot_name + "_HS")

FSRx, FSRy = getBinVal(h_FSR)
PUx, PUy = getBinVal(h_PU)
HSx, HSy = getBinVal(h_HS)
lowx, lowy = getBinVal(h_low)

fig, ax = plt.subplots(figsize=(10, 6))
plt.sca(ax)

#print(FSRx)
#print(FSRy)

PU = plt.step(PUx, PUy, linewidth=2, color = 'navy', label = 'PU')#, align='center', width=5, edgecolor = "red")
HS = plt.step(HSx, HSy, linewidth=2, color = 'dodgerblue', label = 'ISR')
low = plt.step(lowx, lowy, linewidth=2, color = 'orange', label = 'Low ' + '$p_T$')
FSR = plt.step(FSRx, FSRy, linewidth=2, color = 'red', label = 'FSR')


leg = plt.legend(loc='best', fontsize = 16, fancybox=True)
leg.get_frame().set_edgecolor('grey')
#ax.text(x = 10, y = 6.3, s = '0-Lepton selection, 3-jet', fontsize = 16)
#ax.text(x = 10, y = 3.7, s = '0-Lepton selection, 3-jet', fontsize = 16)
ax.text(x = 3.5, y = 1.3, s = '0-Lepton selection, 3-jet', fontsize = 16)

ax.set_ylabel('Yield', fontsize = 16)
#ax.set_xlabel('$m_{bb}$'+ ' [GeV]', fontsize = 16)
#ax.set_xlabel('$m_{bbj}$'+ ' [GeV]', fontsize = 16)
ax.set_xlabel(u'\u0394(b1,j3)+\u0394(b2,j3)', fontsize = 16)

plt.xlim(0, 10)

plt.savefig(plot_name + '_matp' + '.png')
plt.savefig(plot_name + '_matp' + '.pdf')