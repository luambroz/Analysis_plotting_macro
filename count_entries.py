import ROOT
from ROOT import *
import os
import glob

DSIDs_MadGrap = ["361515","361516", "361517", "361518", "361519"]
DSIDs_Sherpa = ["364143","364144","364146","364147","364149","364150","364152","364153","364154","364155"]

for DSID in DSIDs_Sherpa:
  files_paths = glob.glob("/home/ambroz/data3/VHbb/VHbbTruthFramework/ZplusJets/" + DSID + "/group.phys-higgs/*")
  events = 0
  for file_path in files_paths:
    root_file = ROOT.TFile(file_path)
    histo = root_file.Get("h_Keep_SumEntry_" + DSID + "_0Lep")
    events += histo.GetBinContent(1)
    root_file.Close()

  print("DSID " + DSID + ": " + str(events/1e6) + "M")