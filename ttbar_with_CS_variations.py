import os
import plotL

#Create an output directory (if it exists, remove it and create a new one). Then move into it.
plotL.createDir("ttbar_with_CS_variations/")

ttbar_with_CS_variations = {}

DSIDs_ttbar_nominal_and_largest_variation = [410501, 410525]
sys_CS = ["TTbarPTV__1up_CS", "TTbarPTV__1down_CS", "TTbarMBB__1up_CS", "TTbarMBB__1down_CS"]
#####
for jet in plotL.jets:
  for var in plotL.var_names:

    if(var == "pTJ3" and jet == "2"):
      continue
    if(var == "mBBJ" and jet == "2"):
      continue

    ###Set the max error of the first histo, then copy binning to the others. Here target can also be an int > 0###
    target = 0.05
    ###The rebin var will help spread the desired binning to the other histo. At the beginning leave it to 1###
    rebin = 1

    entry = str(jet) + "jet_" +var
    ttbar_with_CS_variations[entry] = []

    for DSID in DSIDs_ttbar_nominal_and_largest_variation:

      HL = plotL.HistoL(
                        DSID,
                        "/home/ambroz/VHbb/VHbbTruthFramework_v2/run/OutputDir" + str(DSID) + "/result" + str(DSID) + ".root",
                        "hist_0Lep_ttbar_2tag" + str(jet) + "jet_150ptv_SR_" + var + "_Nominal",
                        target,
                        rebin = rebin,
                        normalise = True,
                        do_ratio_plot = True
                        )

      ttbar_with_CS_variations[entry].append(HL)

      #Rebin all the systematic like the first one
      rebin = HL.bins

      if (DSID != 410501):
        continue

      for sys in sys_CS:
        HL_CS = plotL.HistoL(
                             DSID,
                             "/home/ambroz/VHbb/VHbbTruthFramework_v2/run/OutputDir" + str(DSID) + "/result" + str(DSID) + ".root",
                             "CS_WeightVariations/hist_0Lep_ttbar_2tag" + str(jet) + "jet_150ptv_SR_" + var + "_" + sys,
                             rebin = rebin,
                             normalise = True,
                             do_ratio_plot = True
                            )

        ttbar_with_CS_variations[entry].append(HL_CS)
#####
plotL.plot(ttbar_with_CS_variations)
os.chdir("../")