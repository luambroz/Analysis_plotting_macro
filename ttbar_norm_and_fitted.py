import os
import plotL

#Create an output directory (if it exists, remove it and create a new one). Then move into it.
plotL.createDir("ttbar_norm_and_fitted/")

ttbar_norm_and_fitted = {}

DSIDs_ttbar_sys = [410511,410512,410225,410525]
#####
for jet in plotL.jets:
  for var in plotL.var_names_for_fit:

    histo410501 = plotL.getHisto("/home/ambroz/VHbb/VHbbTruthFramework_v2/run/OutputDir410501/result410501.root",
                           "hist_0Lep_ttbar_2tag" + str(jet) + "jet_150ptv_SR_" + var + "_Nominal",
                           normalise = True
                          )

    #Set the max error in the bins. Only values: 0 < target < 1.
    target = 0.05
    bins410501 = plotL.computeNewBin(histo410501, target)
    results410501 = plotL.rebinHisto(histo410501, bins410501, plotL.var_range[var]["min"], plotL.var_range[var]["max"], divide_by_bin_width = False)
    histo410501 = results410501[0]
    bins410501  = results410501[1]

    entry = str(jet) + "jet_" +var
    ttbar_norm_and_fitted[entry] = []

    for DSID in DSIDs_ttbar_sys:

      HL = plotL.HistoL(
                        DSID,
                        "/home/ambroz/VHbb/VHbbTruthFramework_v2/run/OutputDir" + str(DSID) + "/result" + str(DSID) + ".root",
                        "hist_0Lep_ttbar_2tag" + str(jet) + "jet_150ptv_SR_" + var + "_Nominal",
                        rebin = bins410501,
                        do_ratio_plot = False,
                        normalise = True,
                        normalise_to_histo = histo410501,
                        set_y_min = 0.5,
                        set_y_max = 1.5,
                        do_fit = True,
                        divide_by_bin_width = False,
                       )
      ttbar_norm_and_fitted[entry].append(HL)
#####
plotL.plot(ttbar_norm_and_fitted)
os.chdir("../")