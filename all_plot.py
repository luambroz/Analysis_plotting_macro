import ROOT
from ROOT import *
from array import array

variables = [
["pt_leading_jet_Nominal", "pt_leading_bjet", "pt_leading_bjet_muon_corr_pt_reco"],
["pt_subleading_jet_Nominal", "pt_subleading_bjet", "pt_subleading_bjet_muon_corr_pt_reco"],
["eta_leading_jet_Nominal", "eta_leading_bjet", "eta_leading_bjet_muon_corr_pt_reco"],
["eta_subleading_jet_Nominal", "eta_subleading_bjet", "eta_subleading_bjet_muon_corr_pt_reco"],
["mbb_Nominal","mbb","mbb_muon_corr_pt_reco"],
["dRbb_Nominal","dRbb","dRbb_muon_corr_pt_reco"],
["n_central_jets_Nominal","n_central_jets","n_central_jets_muon_corr_pt_reco"],
["n_central_bjets_Nominal","n_central_bjets","n_central_bjets_muon_corr_pt_reco"],
["n_central_bjets_cut30GeV_Nominal","n_central_bjets_cut30GeV","n_central_bjets_muon_corr_cut30GeV_pt_reco"],
["pt_leading_bjet_subleading_bjet_Nominal","pt_leading_bjet_subleading_bjet","pt_leading_bjet_subleading_bjet_muon_corr_pt_reco"]
]


gStyle.SetOptStat(0)
gStyle.SetOptTitle(0)

for var in variables:

  c3=TCanvas("c", "c", 900,1200)

  pad_1=TPad("pad_1", "up", 0., 0.6, 1., 1.);
  pad_1.Draw();
  pad_2=TPad("pad_2", "pad_2", 0.0, 0.4, 1.0, 0.6);
  pad_2.Draw();
  pad_3=TPad("pad_3", "pad_3", 0.0, 0.2, 1.0, 0.4);
  pad_3.Draw();
  pad_4=TPad("pad_4", "pad_4", 0.0, 0.00, 1.0, 0.2);
  pad_4.Draw();

  pad_1.cd()

  postfix = ""
  xmin = 0
  xmax = 1
  ymin_r = 0.5
  ymax_r = 1.5
  if "pt" in var[0] or "mbb" in var[0]:
    leg = TLegend( 0.45, 0.6, 0.75, 0.9)
    ymin_r = 0
    ymax_r = 2
    xmin = 0
    xmax = 500
    postfix = " [GeV]"
  elif "central" in var[0]:
    leg = TLegend( 0.45, 0.6, 0.75, 0.9)
    ymin_r = 0
    ymax_r = 2
    xmin = -0.5
    xmax = 15.5
  elif "dRbb" in var[0]:
    xmin = 0
    xmax = 5
    leg = TLegend( 0.35, 0.3, 0.65, 0.6)
  else:
    xmin = -2.5
    xmax = 2.5
    leg = TLegend( 0.35, 0.3, 0.65, 0.6)
  leg.SetShadowColor(10)
  leg.SetFillStyle(0)
  leg.SetBorderSize(0)
  leg.SetTextFont(42)
  leg.SetTextSize(0.020)

  rebin = 1
  if "pt_subleadin" in var[0]:
    rebin = 2


  ###Pad1
  MCVar = "hist_0Lep_qqZvvH125_0ptag2jet_75_150ptv_SR_" + var[0]
  outputTruthWZ = ROOT.TFile("/home/ambroz/VHbb/VHbbTruthFramework_v1/VHbbTruthFramework/macros/NtupleToHist/SignalNoCutsTruthMatchInput26Feb/OutputDir345056WZ/result345056.root")
  hTWZ = outputTruthWZ.Get(MCVar)
  hTWZ.SetDirectory(0)
  outputTruthWZ.Close()
  hTWZ.Rebin(rebin)


  hTWZ.SetLineWidth(3)
  hTWZ.SetLineColor(kBlack)
  hTWZ.GetYaxis().SetTitle("Yields")
  hTWZ.GetYaxis().SetTitleSize(0.035)
  hTWZ.GetYaxis().SetTitleOffset(0.9)
  hTWZ.GetXaxis().SetTitle(var[1] + postfix)

  atlasLabel = TLatex()
  atlasLabel.SetTextFont(42)
  atlasLabel.SetNDC()

  hTWZ.GetXaxis().SetRangeUser(xmin,xmax)

  hTWZ.Draw()



  MCVar = "hist_0Lep_qqZvvH125_0ptag2jet_75_150ptv_SR_" + var[0]
  outputTruthJets = ROOT.TFile("/home/ambroz/VHbb/VHbbTruthFramework_v1/VHbbTruthFramework/macros/NtupleToHist/SignalNoCutsTruthMatchInput26Feb/OutputDir345056Truth/result345056.root")
  hTJets = outputTruthJets.Get(MCVar)
  hTJets.SetDirectory(0)
  outputTruthJets.Close()
  hTJets.Rebin(rebin)


  hTJets.SetLineWidth(3)
  hTJets.SetLineColor(40)
  hTJets.GetYaxis().SetTitle("Yields")
  hTJets.GetYaxis().SetTitleSize(0.035)
  hTJets.GetYaxis().SetTitleOffset(0.9)


  hTJets.Draw("SAME")



  outputCxAOD = ROOT.TFile("/home/ambroz/VHbb/VHbbTruthFramework_v1/VHbbTruthFramework/macros/NtupleToHist/SignalNoCutsTruthMatchInput26Feb/submitDir/hist-user.luambroz.mc16_13TeV.345056.PwPy8EG_NNPDF3_AZNLO_ZH125J_MINLO_vvbb_VpT.r9364.HIGG5D1.30-02_CxAOD.root.root")
  hCR = outputCxAOD.Get(var[1])

  hCR.SetDirectory(0)
  outputCxAOD.Close()
  hCR.Rebin(rebin)


  hCR.SetLineWidth(3)
  hCR.SetLineColor(kAzure+9)
  hCR.GetYaxis().SetTitle("Yields")
  hCR.GetYaxis().SetTitleSize(0.035)
  hCR.GetYaxis().SetTitleOffset(0.9)

  hCR.Draw("SAME")



  outputCxAOD_corr_pt = ROOT.TFile("/home/ambroz/VHbb/VHbbTruthFramework_v1/VHbbTruthFramework/macros/NtupleToHist/SignalNoCutsTruthMatchInput26Feb/submitDir/hist-user.luambroz.mc16_13TeV.345056.PwPy8EG_NNPDF3_AZNLO_ZH125J_MINLO_vvbb_VpT.r9364.HIGG5D1.30-02_CxAOD.root.root")
  hCR_corr_pt  = outputCxAOD_corr_pt.Get(var[2])
  hCR_corr_pt.SetDirectory(0)
  outputCxAOD_corr_pt.Close()
  hCR_corr_pt.Rebin(rebin)

  hCR_corr_pt.SetLineWidth(3)
  hCR_corr_pt.SetLineColor(kOrange)
  hCR_corr_pt.GetYaxis().SetTitle("Yields")
  hCR_corr_pt.GetYaxis().SetTitleSize(0.035)
  hCR_corr_pt.GetYaxis().SetTitleOffset(0.9)

  hCR_corr_pt.Draw("SAME")


  scaleLabel = TLatex()
  scaleLabel.SetTextFont(42)
  scaleLabel.SetNDC()

  leg.AddEntry(hTWZ,  "VHbbTruthFramework TruthJets with WZ four-vector","l")
  leg.AddEntry(hTJets,  "VHbbTruthFramework TruthJets","l")
  leg.AddEntry(hCR, "CxAODFramework RECO ","l")
  leg.AddEntry(hCR_corr_pt, "CxAODFramework RECO Muon in Jet Correction plus PtReco","l")

  leg.Draw("SAME")

  if "pt" in var[0] or "mbb" in var[0] or "central" in var[0]:
    atlasLabel.DrawLatex(0.62,0.42, "#scale[1.0]{#bf{#it{ATLAS}} Internal}")
    scaleLabel.DrawLatex(0.62,0.32, "#scale[0.8]{L=36.1 fb^{-1}}")
  else:
    atlasLabel.DrawLatex(0.35,0.22, "#scale[1.0]{#bf{#it{ATLAS}} Internal}")
    scaleLabel.DrawLatex(0.35,0.12, "#scale[0.8]{L=36.1 fb^{-1}}")

  ###Pad2
  pad_2.cd()
  
  ratioRECO_corr_pt = hCR_corr_pt.Clone("a")
  ratioRECO_corr_pt.Divide(hTWZ)
  ratioRECO_corr_pt.SetMaximum(ymax_r)
  ratioRECO_corr_pt.SetMinimum(ymin_r)
  ratioRECO_corr_pt.GetYaxis().SetTitle("RECO corrected / Truth with WZ")
  ratioRECO_corr_pt.GetYaxis().SetTitleSize(0.06)
  ratioRECO_corr_pt.GetYaxis().SetTitleOffset(0.6)
  ratioRECO_corr_pt.GetXaxis().SetLabelSize(0.12)
  ratioRECO_corr_pt.GetYaxis().SetLabelSize(0.08)
  ratioRECO_corr_pt.GetXaxis().SetTitle(var[1])
  ratioRECO_corr_pt.GetXaxis().SetTitleSize(0.13)
  ratioRECO_corr_pt.GetXaxis().SetRangeUser(xmin,xmax)
  ratioRECO_corr_pt.Draw("SAME")

  line2=TLine(xmin, 1., xmax, 1.)
  line2.SetLineColor(922)
  line2.SetLineWidth(2)
  line2.SetLineStyle(2)
  line2.Draw("SAME")


  ###Pad3
  pad_3.cd()

  ratioRECO = hCR.Clone("b")
  ratioRECO.Divide(hTJets)
  ratioRECO.SetMaximum(ymax_r)
  ratioRECO.SetMinimum(ymin_r)
  ratioRECO.GetYaxis().SetTitle("RECO / TruthJets")
  ratioRECO.GetYaxis().SetTitleSize(0.06)
  ratioRECO.GetYaxis().SetTitleOffset(0.6)
  ratioRECO.GetXaxis().SetLabelSize(0.12)
  ratioRECO.GetYaxis().SetLabelSize(0.08)
  ratioRECO.GetXaxis().SetTitle(var[1] + postfix)
  ratioRECO.GetXaxis().SetTitleSize(0.13)
  ratioRECO.GetXaxis().SetRangeUser(xmin,xmax)
  ratioRECO.Draw()

  line3=TLine(xmin, 1., xmax, 1.)
  line3.SetLineColor(922)
  line3.SetLineWidth(2)
  line3.SetLineStyle(2)
  line3.Draw("SAME")

  ###Pad4
  pad_4.cd()

  ratioRT = hCR_corr_pt.Clone("c")
  ratioRT.Divide(hTJets)
  ratioRT.SetMaximum(ymax_r)
  ratioRT.SetMinimum(ymin_r)
  ratioRT.GetYaxis().SetTitle("RECO Corrected / TruthJets")
  ratioRT.GetYaxis().SetTitleSize(0.06)
  ratioRT.GetYaxis().SetTitleOffset(0.6)
  ratioRT.GetXaxis().SetLabelSize(0.12)
  ratioRT.GetYaxis().SetLabelSize(0.08)
  ratioRT.GetXaxis().SetTitle(var[1] + postfix)
  ratioRT.GetXaxis().SetTitleSize(0.13)
  ratioRT.GetXaxis().SetRangeUser(xmin,xmax)
  ratioRT.SetLineColor(kRed)
  ratioRT.Draw()

  line4=TLine(xmin, 1., xmax, 1.)
  line4.SetLineColor(922)
  line4.SetLineWidth(2)
  line4.SetLineStyle(2)
  line4.Draw("SAME")


  c3.SaveAs(var[1] + ".png")
  #####


